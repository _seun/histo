Histo::Application.routes.draw do
  devise_for :users
  root 'pages#home'
  get "/services" => 'pages#services', as: :services
  get "/pricelist" => 'pages#pricelist', as: :prices
  get "/gallery" => 'pages#gallery', as: :gallery
  get "/contact" => 'pages#contact', as: :contact
  resources :requests
  resources :images

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
