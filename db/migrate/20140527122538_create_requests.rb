class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.text :desc
      t.string :tel
      t.string :email

      t.timestamps
    end
  end
end
