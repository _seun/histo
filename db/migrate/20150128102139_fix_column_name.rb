class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :images, :desc, :description
  end
end
