class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :function
      t.text :desc
      t.string :category

      t.timestamps
    end
  end
end
