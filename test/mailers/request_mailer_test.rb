require 'test_helper'

class RequestMailerTest < ActionMailer::TestCase
  test "request_estimate" do
    mail = RequestMailer.request_estimate
    assert_equal "Request estimate", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
