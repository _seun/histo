class ImagesController < ApplicationController
  before_action :authenticate_user!

  def index
    @title = "Admin | Histo Cakes"
    @images = Image.all
  end

  def new
    @title = "Admin | Histo Cakes"
    @image = Image.new
  end

  def create
    @image= Image.new(image_params)
    if @image.save
     redirect_to new_image_path, notice: "Image uploaded."
    else
     redirect_to new_image_path, alert: "Image was not uploaded."
    end
  end

  def edit
    @title = "Admin | Histo Cakes"
  end

  def update
  end

  private
    def image_params
      params.require(:image).permit(:description, :category, :function, :image)
    end
end
