class RequestsController < ApplicationController
	def new
		@request = Request.new
	end

	def create
		@request = Request.new(request_params)
	  if @request.save
		  redirect_to root_path, notice: "Your request has been recieved. We'll contact you in no time. Thanks"
		else
			redirect_to root_path, notice: "Not done."
		end
	end

	def index
		@requests= Request.all
	end

	private
	
		def request_params
			params.require(:request).permit(:desc, :email, :tel, :photo)
		end
end