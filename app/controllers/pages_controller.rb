class PagesController < ApplicationController
  def home
    @splashImgs = Image.where("function like ?", "%splash%").limit(5).order('created_at desc')
    @latestImgs = Image.where.not(description: 'blank').limit(9).order('created_at desc')
    @request = Request.new
    @title = "Histo Cakes and Catering | Home"
  end

  def services
    @title = "What We Do | Histo Cakes and Catering"
    @s1 = Image.where(:function => 's1').order('created_at desc').first # image at the top of the page
    @sCake = Image.where(:function => 's-cake').order('created_at desc').first # image at the cake section of the page
    @sMeals = Image.where(:function => 's-meal').order('created_at desc').first # image at the meals section of the page
    @sDessert = Image.where(:function => 's-dessert').order('created_at desc').first # image at the dessert section of the page
    @sEvent = Image.where("category like ?", "%Decor%").order('created_at desc').first # image at the event section of the page
  end

  def pricelist
    @title = "Pricelist | Histo Cakes and Catering"
  end

  def gallery
    @title = "Gallery | Histo Cakes and Catering"
    @birthday = Image.where("description like ?", "%birthday%").order('created_at desc')
    @wedding = Image.where("description like ?", "%wed%").order('created_at desc')
    @anniversary = Image.where("description like ?", "%anniv%").order('created_at desc')
    @others = Image.where("category not like ?", "%Cake%").order('created_at desc')
  end

  def contact
    @title = "Contact | Histo Cakes and Catering"
  end
end
