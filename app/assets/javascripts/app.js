$(document).ready(function(){
    $('section#intro .splash img:gt(0)').hide();
    setInterval(function(){
      $('section#intro .splash :first-child').fadeOut()
         .next('img').fadeIn()
         .end().appendTo('section#intro .splash');}, 
      5000);
});

$(function(){
    var $container = $('.gallery');

    $container.imagesLoaded( function(){
      $container.masonry({
        itemSelector : '.gall-img'
      });
    });
});