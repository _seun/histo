class Image < ActiveRecord::Base
  before_save { |image| image.description = image.description.downcase }

	validates :category, presence: true
	has_attached_file :image, :styles => { :medium => '300x300>', :thumb => '100x100>'}
end
