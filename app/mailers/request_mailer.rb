class RequestMailer < ActionMailer::Base
  default from: "info@histocakes.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.request_mailer.request_estimate.subject
  #
  def request_estimate
    @greeting = "Hi"

    mail to: "to@example.org"
    mail :subject => "Mandrill rides the Rails!",
         :to      => "recipient@example.com",
         :from    => "you@yourdomain.com"
  end
end
